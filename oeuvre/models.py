from django.db import models
from auteur.models import Auteur
# Create your models here.


class Oeuvre(models.Model):
    auteur = models.ForeignKey(Auteur, on_delete=models.CASCADE, blank=True)
    titre = models.CharField(max_length=100, verbose_name="titre", blank=True)
    isbn = models.IntegerField(blank=True, null=True)
    description = models.TextField(verbose_name="description", blank=True)

    def __str__(self):
        return self.titre