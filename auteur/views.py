from django.shortcuts import render
from auteur.models import Auteur
# Create your views here.

def index(request):
    return render(request, 'auteur/index.html')

def liste_auteurs(request):
    liste = Auteur.objects.all()
    return render(request, 'auteur/auteurs.html', {'liste':liste})

def auteurDetails(request):
    return render(request, 'auteur/index.html')