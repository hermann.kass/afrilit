from django.contrib import admin
from auteur.models import (
    Auteur
)
# Register your models here.

@admin.register(Auteur)
class AuteurAdmin(admin.ModelAdmin):
    pass